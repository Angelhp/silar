<?php
session_start();
	include "assets/header.html";
	require "assets/menu.html";
	include "assets/panel-med_init.html";
	/*--------------Inicia Contenido----------------------*/
	?>
	<legend>Registro del Usuario</legend>

<!-- HTML -->
        
<form class="card p-2" action="reg_usr.php" method="POST" name="resgistro">
    <div class="col-md-12 order-md-1">
      <h4 class="mb-3">Ingrese la siguiente información</h4>
      
        <div class="mb-3">
          <label for="name">Nombre</label>
          <input type="text" class="form-control" id="name" name="name" placeholder="Nombre" required>
          <!-- <div class="invalid-feedback">
            El nombre es requerido.
          </div> -->
        </div>

        <hr class="mb-4">

        <div class="row">
          <div class="col-md-6 mb-4">
            <label for="user">Usuario</label>
            <input type="text" class="form-control" id="user" name ="user" placeholder="Nombre de Usuario" value="" required>
            <!-- <div class="invalid-feedback">
              El nombre de usuario es requerido
            </div> -->
          </div>
          <div class="col-md-6 mb-3">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="pass" name="pass" placeholder="Contraseña del usuario" value="" required>
            <!-- <div class="invalid-feedback">
              La contraseña es requerida
            </div> -->
          </div>
          
        </div>

        <hr class="mb-4">

        <div class="mb-3">
          <label for="email">Correo electronico</label>
          <div class="">
            
              <span class="input-group-text">@</span>
            
            <input type="text" class="form-control" id="email" name="email" placeholder="Correo" required>
            <!-- <div class="invalid-feedback" style="width: 100%;">
              El correo electrónicoes necesario.
            </div>
          </div> -->
        </div>

        <hr class="mb-4">        
        <div class="mb-3">
          <label for="tel">Teléfono</label>
          <input type="num" class="form-control" id="tel" maxlength="10" name="tel" placeholder="01(246) - " required>
          <!-- <div class="invalid-feedback">
            Necesaro el telefono de contacto.
          </div> -->
        </div>

        </div>
        <hr class="mb-4">
        <div class="form-group">
          <div class="justify-content-center">
            <div class="col-md-6 col-md-offset-3  text-center">
              <button class=" btn btn-primary btn-lg btn-block" type="submit">Registrar</button>
            </div>    
          </div>
        </div>
        
      </form>

      <div>
        <p>  </p>

      </div>


</div>  
	<!-- HTML -->

	<?php

	/*PHP*/

	/*PHP*/



	/*--------------Finaliza Contenido----------------------*/
	include "assets/panel-med_final.html";
  ?>

  
  <!-- <script src="js/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    
  <script>
    window.jQuery || document.write('<script src="js/jquery.slim.min.js"><\/script>')
  </script>
    
  <script src="js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>
    
    <script src="js/form-validation.js"></script> -->

  <?php
	include ("assets/footer.html");
?>