<?php
session_start();
	include "assets/header.html";
	require "assets/menu.html";
	include "assets/panel-med_init.html";
	/*--------------Inicia Contenido----------------------*/
	?>
	<legend>Usuarios Registrados</legend>

	<div>
		<table class="table table-striped table-hover">
  <thead class="thead-dark">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Nombre</th>
      <th scope="col">Usuario</th>
      <th scope="col">Email</th>
      <th scope="col">Tel</th>
      <th scope="col">Acciones</th>

    </tr>
  </thead>
  <?php

  require "inc/conn.php";

  $consult = "SELECT id_user, name, user, email, tel FROM users";

  $query = mysqli_query($link, $consult);

  $row = mysqli_num_rows($query);

  
  echo '<tbody>
    ';

 for ($i=0; $i < $row; $i++) { 
 $data = mysqli_fetch_array($query);	
  echo '<tr>
      <th scope="row">'. $data['id_user'] .'</th>
      <td>'. $data['name'] .'</td>
      <td>'. $data['user'] . '</td>
      <td>'. $data['email'] . '</td>
      <td>'. $data['tel'] .'</td>
      <td><a class="btn btn-primary btn-sm" href="mdf_usr.php?id='. $data['id_user'] .'">Editar</a> <a class="btn btn-danger btn-sm" href="del_usr.php?id='. $data['id_user'] .'">Eliminar</a></td>
      </tr>';
 }

   echo '
  </tbody>
</table>
';

mysqli_close($link);

?>
	</div>

<!-- HTML -->


  <?php
	include ("assets/footer.html");
?>