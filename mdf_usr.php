<?php
session_start();
	include "assets/header.html";
	require "assets/menu.html";
	include "assets/panel-med_init.html";
	/*--------------Inicia Contenido----------------------*/

  $id = $_GET['id'];

  require "inc/conn.php";

  $cons = "SELECT name, user, email, tel FROM users WHERE id_user ='" . $id ."'";

  $query = mysqli_query($link, $cons);

  $data = mysqli_fetch_array($query);


	?>
	<legend>Modificar datos del usuario</legend>

<!-- HTML -->
        
<form class="card p-2" action="upd_usr.php" method="POST" name="registro">
    <div class="col-md-12 order-md-1">
      <h4 class="mb-3">Modifique los campos pertinentes</h4>
      
        <div class="mb-3">
          <label for="name">Nombre</label>
          <?php

          echo '<input type="hidden" id="id" name="id" value="' . $id . '">';

          echo '
          <input type="text" class="form-control" id="name" name="name" placeholder="Nombre" required value="' . $data['name'] . '">
          ';
          ?>
        </div>

        <hr class="mb-4">

        <div class="row">
          <div class="col-md-6 mb-4">
            <label for="user">Usuario</label>
          <?php
          echo '
            <input type="text" class="form-control" id="user" name ="user" placeholder="Nombre de Usuario" value="' . $data['user'] . '">
            '
           ?> 
          </div>
          <div class="col-md-6 mb-3">
            <label for="password">Password</label>
            <input disabled type="password" class="form-control" id="pass" name="pass" placeholder="Contraseña del usuario" value="" required>
            <!-- <div class="invalid-feedback">
              La contraseña es requerida
            </div> -->
          </div>
          
        </div>

        <hr class="mb-4">

        <div class="mb-3">

          <label for="email">Correo electronico</label>
          <div class="">
            
              <span class="input-group-text">@</span>
            <?php
            echo '
            <input type="text" class="form-control" id="email" name="email" placeholder="Correo" value="' . $data['email'] . '" >
            '
           ?> 
        </div>

        <hr class="mb-4">        
        <div class="mb-3">
          <label for="tel">Teléfono</label>
           <?php
            echo '
          <input type="num" class="form-control" id="tel" maxlength="10" name="tel" placeholder="01(246) - "  value="' . $data['tel'] . '">
            '
           ?>
        </div>

        </div>
        <hr class="mb-4">
        <div class="form-group">
          <div class="justify-content-center">
            <div class="col-md-6 col-md-offset-3  text-center">
              <button class=" btn btn-success btn-lg btn-block" type="submit">Guardar</button>
            </div>    
          </div>
        </div>
        
      </form>

      <div>
        <p>  </p>

      </div>


</div>  
	<!-- HTML -->

	<?php

  mysqli_close($link);

	/*--------------Finaliza Contenido----------------------*/
	include "assets/panel-med_final.html";
	include ("assets/footer.html");
?>